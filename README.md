# Minimal Boilerplates for Web and Smart Contract Development

| Folder      | Explanation                                                    |
| ----------- | -------------------------------------------------------------- |
| `bootstrap` | Static website with Bootstrap and jQuery                       |
| `vue`       | Static website with Vue.js and TypeScript                      |
| `solidity`  | Ethereum smart contract with Solidity, Hardhat, and TypeScript |

## Usage

```bash
cp boilerplates/{bootstrap,vue,solidity}/* /path/to/new/project
cd /path/to/new/project
npm install
```

## Resources

- [Introduction · Bootstrap v5.0](https://getbootstrap.com/docs/5.0/getting-started)
- [Bootstrap Icons · Official open source SVG icon library for Bootstrap](https://icons.getbootstrap.com/)
- [Introduction | Vue.js](https://v3.vuejs.org/guide/introduction.html)
- [TypeScript Support | Vue.js](https://v3.vuejs.org/guide/typescript-support.html)
- [Style Guide | Vue.js](https://v3.vuejs.org/style-guide/)
- [Solidity — Solidity 0.6.12 documentation](https://docs.soliditylang.org/en/v0.6.12/)
- [Contracts - OpenZeppelin Docs](https://docs.openzeppelin.com/contracts/3.x/)
- [Overview | Hardhat | Ethereum development environment for professionals by Nomic Labs](https://hardhat.org/getting-started/)
