// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.6.12;

contract HelloWorld {
    function renderHelloWorld() public pure returns (string memory) {
        return "Hello World!";
    }
}
