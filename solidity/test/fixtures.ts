import { ethers, Signer, parseEther } from "./helpers";

import { HelloWorld } from "../typechain";

const setupTest = async (deployer: Signer) => {
  const HelloWorldFactory = await ethers.getContractFactory(
    "HelloWorld",
    deployer
  );
  const HelloWorld = (await HelloWorldFactory.deploy()) as HelloWorld;

  return { HelloWorld };
};

export { setupTest };
