import { ethers, Signer, expect } from "./helpers";
import { setupTest } from "./fixtures";
import { HelloWorld } from "../typechain";

describe("HelloWorld", async () => {
  let deployer: Signer;

  let HelloWorld: HelloWorld;

  before(async () => {
    [deployer] = await ethers.getSigners();
  });

  beforeEach(async () => {
    ({ HelloWorld } = await setupTest(deployer));
  });

  it("should render hello world string", async () => {
    const helloWorldString = await HelloWorld.connect(
      deployer
    ).renderHelloWorld();
    expect(helloWorldString).to.equal("Hello World!");
  });
});
