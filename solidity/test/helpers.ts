// Ethers
import { ethers, upgrades } from "hardhat";
import { Signer } from "ethers";

const parseEther = ethers.utils.parseEther;
const bn = ethers.BigNumber.from;

// Chai
import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import { solidity } from "ethereum-waffle";

chai.use(chaiAsPromised);
chai.use(solidity);
const { assert, expect } = chai;

export { ethers, upgrades, Signer, parseEther, bn, assert, expect };
